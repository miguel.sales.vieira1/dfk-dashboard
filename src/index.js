import React from 'react';
import ReactDOM from 'react-dom';
import { ethers } from "ethers";
import App from './screens/App';
import 'antd/dist/antd.css';
import "./index.scss";

const chainId = "1666600000";
const library = new ethers.providers.JsonRpcProvider("https://api.s0.t.hmny.io/");

ReactDOM.render(
  <React.StrictMode>
    <App chainId={chainId} library={library} />
  </React.StrictMode>,
  document.getElementById('root')
);
