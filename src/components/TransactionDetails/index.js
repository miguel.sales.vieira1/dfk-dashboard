import { CustomBlock } from "../../components";
import { Card, Avatar, Tabs } from 'antd';
import { getCustomContracts } from "../../utils/api/contracts";

const { Meta } = Card;
const { TabPane } = Tabs;

function onChageTab(key) {
    console.log(key);
}

function TransactionDetails({ transactions, account, chainId, loaded }) {

    const contracts = getCustomContracts(chainId);
    if (!loaded)
        return <div className='TransactionDetailsWrapper'>
            <h2>Transaction Details</h2>
            <Card style={{ width: "100%", marginTop: 16 }} loading={true}>
                <Meta
                    avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                    title="Card title"
                    description="This is the description"
                />
            </Card>
        </div>;
    else {
        return <div className="TransactionDetailsWrapper">
            <h2>Transaction Details</h2>
            <Tabs defaultActiveKey="1" onChange={onChageTab}>
                {contracts && contracts.length > 0 && contracts.map((contract) => {
                    return <TabPane tab={contract.name} key={contract.address}>
                        <CustomBlock
                            address={contract.address}
                            transactions={transactions}
                            chainId={chainId}
                            account={account}
                            loaded={loaded}
                        />
                    </TabPane>
                })}
            </Tabs>
        </div>
    }
}
export default TransactionDetails;

