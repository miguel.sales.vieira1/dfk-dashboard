import moment from "moment";
import CsvDownloader from 'react-csv-downloader';
import { Button, DatePicker } from 'antd';
import { DownloadOutlined } from '@ant-design/icons';
import { getAddress } from "@harmony-js/crypto";
import { formatUnits } from '@ethersproject/units';
import { getTokenName, getTokenDecimals } from '../../utils/api/tokens';
import { getContractName } from "../../utils/api/contracts.js";
import "./styles.scss";

const { RangePicker } = DatePicker;

const columns = [{
    id: 'date',
    displayName: 'Date'
}, {
    id: 'time',
    displayName: 'Time'
}, {
    id: 'fromAddress',
    displayName: 'From Address'
}, {
    id: 'fromName',
    displayName: 'From Name'
}, {
    id: 'toAddress',
    displayName: 'To Address'
}, {
    id: 'toName',
    displayName: 'To Name'
}, {
    id: 'quantity',
    displayName: 'Quantity'
}, {
    id: 'tokenAddress',
    displayName: 'Token Address'
}, {
    id: 'tokenName',
    displayName: 'Token Name'
}];



function Header({ currentTransactions, allTransactions, setTransactions, account, chainId }) {
    const asyncFnComputeDate = (transactions) => {
        let newTransactions = [];
        for (let i = 0; i < transactions.length; i++) {
            const transaction = transactions[i];
            const transferLogs = transaction.logs.filter((log) => log.name === "Transfer");
            for (let j = 0; j < transferLogs.length; j++) {
                const log = transferLogs[j];
                const from = log.inputs[0].value;
                const to = log.inputs[1].value;
                const value = log.inputs[2].value;
                const units = getTokenDecimals(log.tokenAddress, chainId);
                const result = formatUnits(value, units);

                const isFromYou = log.inputs[0].value.toLowerCase() === getAddress(account).basicHex.toLowerCase();
                const isToYou = log.inputs[1].value.toLowerCase() === getAddress(account).basicHex.toLowerCase();
                const isMint = log.inputs[0].value.toLowerCase() === "0x0000000000000000000000000000000000000000";
                const isBurn = log.inputs[1].value.toLowerCase() === "0x0000000000000000000000000000000000000000";

                const fromName = isFromYou ? "YOU" : isMint ? "MINT" : getContractName(log.inputs[0].value, chainId);
                const toName = isToYou ? "YOU" : isBurn ? "BURN" : getContractName(log.inputs[1].value, chainId);

                const item = {
                    date: moment.unix(transaction.timestamp).format("MM/DD/YYYY"),
                    time: moment.unix(transaction.timestamp).format("HH:mm"),
                    fromAddress: from,
                    fromName: fromName,
                    toAddress: to,
                    toName: toName,
                    quantity: result,
                    tokenAddress: log.tokenAddress,
                    tokenName: getTokenName(log.tokenAddress, chainId)
                }
                newTransactions.push(item);
            }
        }
        return Promise.resolve(newTransactions);
    };

    return (<div className='HeaderComponentWrapper'>
        <RangePicker onChange={(dates, dateStrings) => {
            if (!dates) {
                setTransactions(allTransactions);
            }
            else {
                const filteredTransactions = allTransactions.filter((transaction) => {
                    const time = moment.unix(transaction.timestamp);
                    return time.isBetween(dates[0], dates[1]);
                })
                setTransactions(filteredTransactions);
            }
        }} />
        <CsvDownloader filename="report"
            extension=".csv"
            separator=";"
            wrapColumnChar="'"
            columns={columns}
            datas={() => asyncFnComputeDate(currentTransactions)}
        >
            <Button type="primary" icon={<DownloadOutlined />}>
                Get Report
            </Button>
        </CsvDownloader>
    </div>)
}

export default Header;

