import "./styles.scss"


function TokenItem({ token, quantity, valueJewel, valueUsd }) {
    return <div className="TokenItemWrapper">
        <div className='ImageWrapper'>
            <div className='Image' />
        </div>
        <div className='InfoWrapper'>
            <div className='NameQuantityWrapper'>
                <p className="Name">{token?.name || "No Token"}</p>
                <p className="Quantity">{token?.decimals === 0 ? quantity : parseFloat(quantity).toFixed(2)}</p>
            </div>
            <p className="Price">{valueJewel !== '-' ? parseFloat(valueJewel).toFixed(4) + " Jewel" : valueJewel}</p>
            <p className="Price">{valueUsd !== '-' ? "$ " + parseFloat(valueUsd).toFixed(2) : valueUsd}</p>
        </div>
    </div>
}

export default TokenItem;

