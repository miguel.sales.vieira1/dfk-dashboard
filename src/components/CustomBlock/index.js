import React, { useState, useEffect } from "react";
import { formatUnits } from '@ethersproject/units'
import { getAddress } from "@harmony-js/crypto";
import { getTokenName, getTokenDecimals } from "../../utils/api/tokens";
import { List, Popover, Tag } from 'antd';
import moment from "moment";
import "./styles.scss";

const { CheckableTag } = Tag;


function CustomBlock({ address, transactions, chainId, account, loaded }) {
    const [tokenTransactions, settokenTransactions] = useState([]);
    const [actions, setActions] = useState([]);
    const [selectedActions, setSelectedActions] = useState([]);
    const [pageSize, setPageSize] = useState(10);

    useEffect(() => {
        if (!chainId || !account)
            return;

        const filteredTokenTransactions = transactions?.filter((transaction) => {
            const tokenAddress = getAddress(address).basicHex;
            const fromEthAddress = getAddress(transaction.from).basicHex;
            const toEthAddress = getAddress(transaction.to).basicHex;
            if (tokenAddress.toLowerCase() === fromEthAddress.toLowerCase() || tokenAddress.toLowerCase() === toEthAddress.toLowerCase()) {
                return transaction;
            } else {
                return null;
            }
        });

        let actions = [];
        for (var i = 0; i < filteredTokenTransactions.length; i++) {
            const logs = filteredTokenTransactions[i].logs;
            for (var j = 0; j < logs.length; j++) {
                actions.push(logs[j].name);
            }
        }
        const uniqueActions = [...new Set(actions)];

        settokenTransactions(filteredTokenTransactions);
        setActions(uniqueActions);
    }, [account, address, chainId, transactions]);

    const renderLog = (log, index) => {
        const content = <div className="Inputs">
            {log.inputs.map((input, _index) => {
                let valueElement = null;

                if (input.type === "uint256" && input.value.hex.length > 16) {
                    const value = input.value;
                    const units = getTokenDecimals(log.tokenAddress, chainId);
                    const result = formatUnits(value, units);

                    valueElement = <p className="InputValue">
                        {units === 0 ? result : parseFloat(result).toFixed(2)}
                    </p>
                } else if (input.type === "uint256") {
                    valueElement = <p className="InputValue">
                        {formatUnits(input.value, 0)}
                    </p>
                } else if (input.type === "uint64") {
                    valueElement = <p className="InputValue">
                        {formatUnits(input.value, 0)}
                    </p>
                } else if (input.type === "uint16" || input.type === "uint8") {
                    valueElement = <p className="InputValue">
                        {input.value}
                    </p>
                } else if (input.type === "tuple") {
                    // TODO : CHECK THIS ONE
                    // valueElement = <p className="InputValue">
                    //     {"tuple"}
                    // </p>
                    valueElement = null;
                } else {
                    valueElement = <p className="InputValue" style={{
                        maxWidth: "120px",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        cursor: "pointer"
                    }}>{
                            input.value.toLowerCase() === getAddress(account).basicHex.toLowerCase() ? "YOU" :
                                getTokenName(input.value, chainId)}</p>
                }

                return <div className="InputWrapper" key={_index}>
                    <p className="InputName">{input.name}</p>
                    {valueElement}
                </div>
            })}
            {/* {log.tokenAddress && <p>{getAddressName(log.tokenAddress, chainId)}</p>} */}
        </div>;
        return <div className="LogWrapper" key={index}>
            <Popover content={content} trigger="hover">
                <p className="LogName">{log.name}</p>
            </Popover>
        </div>
    }

    const selectedTransactions = selectedActions && selectedActions.length > 0 ? tokenTransactions.map((transaction) => {
        const selectedLogs = transaction.logs.filter((log) => {
            if (selectedActions.includes(log.name))
                return log;
            else
                return null;
        });
        if (selectedLogs && selectedLogs.length > 0)
            return { ...transaction, logs: selectedLogs };
        else
            return undefined;
    }) : tokenTransactions;
    const filteredselectedTransactions = selectedTransactions.filter(function (x) {
        return x !== undefined;
    });


    return <div className="CustomBlockWrapper">
        <div className="TableWrapper">
            <div className="Header">
                {actions.map(action => (
                    <CheckableTag
                        key={action}
                        checked={selectedActions.indexOf(action) > -1}
                        onChange={checked => {
                            const nextSelectedTags = checked ? [...selectedActions, action] : selectedActions.filter(t => t !== action);
                            setSelectedActions(nextSelectedTags);
                        }}
                    >
                        {action}
                    </CheckableTag>
                ))}
            </div>
            <List
                itemLayout="vertical"
                size="large"
                pagination={{
                    onShowSizeChange: (_, size) => {
                        setPageSize(size);
                    },
                    showSizeChanger: true,
                    pageSize: pageSize,
                }}
                dataSource={filteredselectedTransactions}
                renderItem={(item, index) => {
                    return <div>
                        <div className="HeaderWrapper" style={index === 0 ? { borderTop: "none" } : {}}>
                            <div className="HeaderItem" style={{ flex: "1" }}>
                                <p className="Title">Date</p>
                                <p className="Value">{moment.unix(item.timestamp).format("MMMM Do YYYY")}</p>
                            </div>
                            <div className="HeaderItem">
                                <p className="Title">Nonce</p>
                                <p className="Value">{item.nonce}</p>
                            </div>
                            <div className="HeaderItem">
                                <p className="Title">Gas used</p>
                                <p className="Value">{formatUnits(item.gasUsed, 8)}</p>
                            </div>
                        </div>
                        <div className="LogsWrapper">
                            {item.logs && item.logs.map((log, index) => {
                                return renderLog(log, index);
                            })}
                        </div>
                    </div>
                }}
            />
        </div>
    </div>
}

export default CustomBlock;