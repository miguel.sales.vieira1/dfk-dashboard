import React, { useState, useEffect } from "react";
import { Spin, Card, Avatar } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { formatUnits } from '@ethersproject/units'

import { getToken, getTokenQuantity, getLockedJewelQuantity } from "../../utils/api/tokens";
import { getJEWELPair, getUSDPair, getPairPrice } from "../../utils/api/pairs";
import { getContractByName } from "../../utils/api/contracts";
import tokens from "../../assets/tokens.json";
import "./styles.scss";

const { Meta } = Card;
const antIcon = <LoadingOutlined style={{ fontSize: 24, color: "#29af57" }} spin />;

function MainInfo({ account, chainId, library, blockNumber }) {
    const [values, setValues] = useState([]);
    const [firstLoad, setFirstLoad] = useState(true);
    const [loading, setLoading] = useState(false);

    const allTokens = tokens[chainId]?.main;

    useEffect(() => {
        async function getData() {
            if (chainId && library && account && allTokens) {
                setLoading(true);
                let values = [];
                for (let i = 0; i < allTokens.length; i++) {
                    const token = getToken(allTokens[i], chainId);
                    const tokenAddress = token.address;
                    const tokenName = token.name;

                    const jewelContract = getContractByName("Jewel", chainId);
                    if (jewelContract.address.toLowerCase() === tokenAddress.toLowerCase()) {
                        const quantity = await getTokenQuantity(tokenAddress.toLowerCase(), account, chainId, library);
                        const quantityLocked = await getLockedJewelQuantity(account, chainId, library);
                        const formattedQuantity = formatUnits(quantity ? quantity : 0, token.decimals);
                        const formattedQuantityLocked = formatUnits(quantityLocked ? quantityLocked : 0, token.decimals);

                        const usdPair = getUSDPair(tokenAddress, chainId);
                        let valueUsd = "-";
                        let valueUsdLocked = "-";
                        if (usdPair && usdPair !== undefined) {
                            const token0 = getToken(usdPair.token0Address, chainId);
                            const token1 = getToken(usdPair.token1Address, chainId);
                            const price = await getPairPrice(usdPair.address, token0.decimals, token1.decimals, chainId, library);
                            valueUsd = price ? parseFloat(formattedQuantity * price).toFixed(2) : "-";
                            valueUsdLocked = price ? parseFloat(formattedQuantityLocked * price).toFixed(2) : "-";
                        }
                        values.push({
                            valueJewel: parseFloat(formattedQuantity).toFixed(2),
                            valueUsd: valueUsd,
                            quantity: parseFloat(formattedQuantity).toFixed(2),
                            token: token,
                            name: "Wallet Jewel"
                        });

                        values.push({
                            valueJewel: parseFloat(formattedQuantityLocked).toFixed(2),
                            valueUsd: valueUsdLocked,
                            quantity: parseFloat(formattedQuantityLocked).toFixed(2),
                            token: token,
                            name: "Locked Jewel"
                        });
                    } else {
                        const jewelPair = getJEWELPair(tokenAddress, chainId);
                        if (jewelPair && jewelPair !== undefined) {
                            const isTokenJewel = tokenAddress.toLowerCase() === jewelPair.token0Address.toLowerCase();
                            const token0 = getToken(jewelPair.token0Address, chainId);
                            const token1 = getToken(jewelPair.token1Address, chainId);
                            const price = await getPairPrice(jewelPair.address, token0.decimals, token1.decimals, chainId, library);
                            const quantity = await getTokenQuantity(tokenAddress.toLowerCase(), account, chainId, library);
                            const formattedQuantity = formatUnits(quantity ? quantity : 0, isTokenJewel ? token0.decimals : token1.decimals);

                            let value = 0;
                            if (isTokenJewel) {
                                value = price ? parseFloat(formattedQuantity * price) : "-";
                            } else {
                                value = price ? parseFloat(formattedQuantity * (1 / price)) : "-";
                            }

                            let valueUsd = "-";
                            const usdPair = getUSDPair(jewelContract.address, chainId);

                            if (usdPair && usdPair !== undefined) {
                                const token0Usd = getToken(usdPair.token0Address, chainId);
                                const token1Usd = getToken(usdPair.token1Address, chainId);
                                const priceJewelUsd = await getPairPrice(usdPair.address, token0Usd.decimals, token1Usd.decimals, chainId, library);
                                valueUsd = priceJewelUsd ? parseFloat(value * priceJewelUsd) : "-";
                            }

                            values.push({
                                valueJewel: value,
                                valueUsd: valueUsd,
                                quantity: formattedQuantity,
                                token: isTokenJewel ? token0 : token1,
                                name: tokenName
                            });
                        } else {
                            const token0 = getToken(tokenAddress, chainId);
                            const quantity = await getTokenQuantity(tokenAddress.toLowerCase(), account, chainId, library);
                            const formattedQuantity = formatUnits(quantity ? quantity : 0, token0.decimals);

                            values.push({
                                valueJewel: "-",
                                valueUsd: "-",
                                quantity: formattedQuantity,
                                token: token0,
                                name: tokenName
                            });
                        }
                    }
                }
                setValues(values);
                setLoading(false);
                setFirstLoad(false);
            }
        }
        getData();
    }, [library, chainId, account, allTokens, blockNumber]);

    if (firstLoad) {
        return <div className='TokensListWrapper'>
            <h2>Transactions History</h2>
            <Card style={{ width: "100%", marginTop: 16 }} loading={true}>
                <Meta
                    avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                    title="Card title"
                    description="This is the description"
                />
            </Card>
        </div>;
    }

    const totalValueJewel = values?.map((value) => {
        if (isNaN(value.valueJewel))
            return 0;
        else
            return parseFloat(value.valueJewel)
    }).reduce((totalValue, currentValue) => {
        return parseFloat(totalValue) + parseFloat(currentValue);
    }, 0);

    const totalValueUsd = values?.map((value) => {
        if (isNaN(value.valueUsd))
            return 0;
        else
            return parseFloat(value.valueUsd)
    }).reduce((totalValue, currentValue) => {
        return parseFloat(totalValue) + parseFloat(currentValue);
    }, 0);

    return <div className="MainInfoWrapper">
        {!loading && <h2>Account Info</h2>}
        {loading && <div style={{ display: "flex", alignItems: "flex-end" }}><h2>Account Info</h2><Spin indicator={antIcon} style={{ marginLeft: "12px", marginBottom: "14px" }} /></div>}
        <div className="ValuesWrapper">
            {values && values.length > 0 && values.map((value, index) => {
                return <div className='InfoWrapper' key={"mainInfoWrapper" + index}>
                    <div className='NameQuantityWrapper'>
                        <p className="Name">{value?.name || "No Token"}</p>
                        <p className="Quantity">{value?.token?.decimals === 0 ? value?.quantity : parseFloat(value?.quantity).toFixed(2)}</p>
                    </div>
                    <p className="Price">{value?.valueJewel !== '-' ? parseFloat(value?.valueJewel).toFixed(4) + " Jewel" : value?.valueJewel}</p>
                    <p className="Price">{value?.valueUsd !== '-' ? "$ " + parseFloat(value?.valueUsd).toFixed(2) : value?.valueUsd}</p>
                </div>
            })}
        </div>
        <div className="MainInfoFooter">
            <p className="Value" style={{ flex: "1", textAlign: "start" }}>Total</p>
            <p className="Value">{parseFloat(totalValueJewel).toFixed(4) + " Jewel"}</p>
            <p className="Value">{"$ " + parseFloat(totalValueUsd).toFixed(2)}</p>
        </div>
    </div>
}

export default MainInfo;

