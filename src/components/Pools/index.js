import React, { useState, useEffect } from "react";
import { ethers } from "ethers";
import { getUSDPair, getJEWELPair, getPairPrice, getPairFromAddress } from "../../utils/api/pairs";
import { getToken } from "../../utils/api/tokens";
import { getContractByName } from "../../utils/api/contracts";
import { formatUnits } from '@ethersproject/units';
import { Spin, Card, Avatar } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

import "./styles.scss";

const { Meta } = Card;

const antIcon = <LoadingOutlined style={{ fontSize: 24, color: "#29af57" }} spin />;

function Pools({ account, chainId, library, blockNumber }) {
    const [values, setValues] = useState([]);
    const [loading, setLoading] = useState(false);
    const [firstLoad, setFirstLoad] = useState(true);

    useEffect(() => {
        async function getData() {
            if (chainId && library && account) {
                setLoading(true);
                const poolsContractInfo = getContractByName("Pools", chainId);
                const poolsInterface = new ethers.utils.Interface(poolsContractInfo.abi);
                const poolsContract = new ethers.Contract(poolsContractInfo.address, poolsInterface, library);

                const poolLength = await poolsContract.poolLength();
                const formattedPoolLength = formatUnits(poolLength, 0);
                const newValues = [];
                for (let i = 0; i < formattedPoolLength; i++) {
                    const { lpToken } = await poolsContract.poolInfo(i);
                    const pair = getPairFromAddress(lpToken, chainId);

                    if (!pair || pair.undefined)
                        continue;

                    const pairContractInfo = getContractByName("UniswapV2Pair", chainId);
                    const pairInterface = new ethers.utils.Interface(pairContractInfo.abi);
                    const pairContract = new ethers.Contract(lpToken, pairInterface, library);

                    const { amount } = await poolsContract.userInfo(i, account);
                    const reserves = await pairContract.getReserves();
                    const totalSupply = await pairContract.totalSupply();
                    const pendingReward = await poolsContract.pendingReward(i, account);

                    const token0 = getToken(pair.token0Address, chainId);
                    const token1 = getToken(pair.token1Address, chainId);

                    const reserve0 = formatUnits(reserves.reserve0, 18);
                    const reserve1 = formatUnits(reserves.reserve1, 18);
                    const formattedAmount = formatUnits(amount, pair.decimals);
                    const formattedTotalSupply = formatUnits(totalSupply, 18);
                    const formattedPendingReward = formatUnits(pendingReward, 18);

                    const userRatio = formattedAmount / formattedTotalSupply;
                    const token0Amount = userRatio * reserve0;
                    const token1Amount = userRatio * reserve1;
                    const share = userRatio * 100;

                    let valueJewel = 0;

                    const jewelContract = getContractByName("Jewel", chainId);
                    const isTokenJewel0 = token0.address.toLowerCase() === jewelContract.address;
                    const isTokenJewel1 = token1.address.toLowerCase() === jewelContract.address;

                    if (isTokenJewel0) {
                        valueJewel += token0Amount;
                    } else {
                        const jewelPairToken0 = getJEWELPair(token0.address, chainId);
                        const token0PairToken0 = getToken(jewelPairToken0.token0Address, chainId);
                        const token1PairToken0 = getToken(jewelPairToken0.token1Address, chainId);
                        const price = await getPairPrice(jewelPairToken0.address, token0PairToken0.decimals, token1PairToken0.decimals, chainId, library);
                        if (token0PairToken0.address.toLowerCase() === token0.address.toLowerCase())
                            valueJewel += parseFloat(price) * parseFloat(token0Amount);
                        else
                            valueJewel += (1 / parseFloat(price)) * parseFloat(token0Amount);
                    }

                    if (isTokenJewel1) {
                        valueJewel += token1Amount;
                    } else {
                        const jewelPairToken1 = getJEWELPair(token1.address, chainId);
                        const token0PairToken1 = getToken(jewelPairToken1.token0Address, chainId);
                        const token1PairToken1 = getToken(jewelPairToken1.token1Address, chainId);
                        const price = await getPairPrice(jewelPairToken1.address, token0PairToken1.decimals, token1PairToken1.decimals, chainId, library);
                        if (token0PairToken1.address.toLowerCase() === token1.address.toLowerCase())
                            valueJewel += parseFloat(price) * parseFloat(token1Amount);
                        else
                            valueJewel += (1 / parseFloat(price)) * parseFloat(token1Amount);
                    }

                    let valueUsd = 0;
                    const usdPair = getUSDPair(jewelContract.address, chainId);

                    if (usdPair && usdPair !== undefined) {
                        const token0Usd = getToken(usdPair.token0Address, chainId);
                        const token1Usd = getToken(usdPair.token1Address, chainId);
                        const priceJewelUsd = await getPairPrice(usdPair.address, token0Usd.decimals, token1Usd.decimals, chainId, library);
                        valueUsd = parseFloat(priceJewelUsd) * parseFloat(valueJewel);
                    }


                    newValues.push({
                        quantity: formattedAmount,
                        name: token0.symbol + " / " + token1.symbol,
                        address: lpToken,
                        decimals: pair.decimals,
                        token0: token0,
                        token0Amount: token0Amount,
                        token1: token1,
                        token1Amount: token1Amount,
                        pendingReward: formattedPendingReward,
                        sharePercentage: share,
                        valueJewel: valueJewel,
                        valueUsd: valueUsd
                    });
                }
                setValues(newValues);
                setLoading(false);
                setFirstLoad(false);
            }
        }
        getData();
    }, [library, chainId, account, blockNumber]);

    const totalValueJewel = values?.map((value) => {
        if (isNaN(value.valueJewel))
            return 0;
        else
            return parseFloat(value.valueJewel)
    }).reduce((totalValue, currentValue) => {
        return parseFloat(totalValue) + parseFloat(currentValue);
    }, 0);

    const totalValueUsd = values?.map((value) => {
        if (isNaN(value.valueUsd))
            return 0;
        else
            return parseFloat(value.valueUsd)
    }).reduce((totalValue, currentValue) => {
        return parseFloat(totalValue) + parseFloat(currentValue);
    }, 0);

    if (firstLoad)
        return <div className='PoolsWrapper'>
            <h2>Gardens</h2>
            <Card style={{ width: "100%", marginTop: 16 }} loading={true}>
                <Meta
                    avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                    title="Card title"
                    description="This is the description"
                />
            </Card>
        </div>;


    return <div className="PoolsWrapper">
        {!loading && <h2>Gardens</h2>}
        {loading && <div style={{ display: "flex", alignItems: "flex-end" }}><h2>Gardens</h2><Spin indicator={antIcon} style={{ marginLeft: "12px", marginBottom: "14px" }} /></div>}
        <div className="PoolsListWrapper">
            {values && values.length > 0 && values.map((value) => {
                // if (parseFloat(value.quantity) === 0)
                //     return null;
                const hasQuantity = parseFloat(value.quantity) > 0
                return <div key={value.address} className="PoolItemWrapper">
                    <div className="PoolItemHeader">
                        <p className="Name">{value.name}</p>
                        <p className="Address">{value.address}</p>
                    </div>
                    {hasQuantity && <div className="PoolItemBody">
                        <div className="ValuesWrapper">
                            <div className="ValueWrapper">
                                <p className="Name">Amount</p>
                                <p className="Amount">{parseFloat(value.quantity).toFixed(6)}</p>
                            </div>
                            <div className="ValueWrapper">
                                <p className="Name">Your share</p>
                                <p className="Amount">{parseFloat(value.sharePercentage).toFixed(6) + "%"}</p>
                            </div>
                            <div className="ValueWrapper">
                                <p className="Name">Pending Rewards</p>
                                <p className="Amount">{parseFloat(value.pendingReward).toFixed(6) + " Jewel"}</p>
                            </div>
                        </div>
                        <div className="TokensWrapper">
                            <div className="TokenWrapper">
                                <p className="Symbol">{value.token0.symbol}</p>
                                <p className="Amount">{parseFloat(value.token0Amount).toFixed(6)}</p>
                            </div>
                            <div className="TokenWrapper">
                                <p className="Symbol">{value.token1.symbol}</p>
                                <p className="Amount">{parseFloat(value.token1Amount).toFixed(6)}</p>
                            </div>
                        </div>
                    </div>
                    }
                    <div className="PoolItemFooter">
                        {hasQuantity && <p className="Value">{parseFloat(value.valueJewel).toFixed(4) + " Jewel"}</p>}
                        {hasQuantity && <p className="Value">{"$ " + parseFloat(value.valueUsd).toFixed(2)}</p>}
                        {!hasQuantity && <p className="Value">No LP tokens</p>}
                    </div>
                </div>
            })}
        </div>
        <div className="PoolsListFooter">
            <p className="Value" style={{ flex: "1", textAlign: "start" }}>Total</p>
            <p className="Value">{parseFloat(totalValueJewel).toFixed(4) + " Jewel"}</p>
            <p className="Value">{"$ " + parseFloat(totalValueUsd).toFixed(2)}</p>
        </div>
    </div>
}

export default Pools;

