import React, { useState, useEffect } from "react";
import { getUSDPair, getJEWELPair, getPairPrice } from "../../utils/api/pairs";
import { getCustomTokenAddresses, getToken, getTokenQuantity } from "../../utils/api/tokens";
import { getContractByName } from "../../utils/api/contracts";
import { TokensList } from "../../components";
import { formatUnits } from '@ethersproject/units'
import { Card, Avatar, Tabs, Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

const { Meta } = Card;
const antIcon = <LoadingOutlined style={{ fontSize: 24, color: "#29af57" }} spin />;

const { TabPane } = Tabs;
function onChageTab(key) {
    console.log(key);
}

function Tokens({ account, chainId, library, blockNumber }) {
    const [values, setValues] = useState([]);
    const [loading, setLoading] = useState(false);
    const [firstLoad, setFirstLoad] = useState(true);

    const tokens = getCustomTokenAddresses(chainId);

    useEffect(() => {
        async function getData() {
            if (chainId && library && account && tokens) {
                setLoading(true);
                const allTokens = Object.values(tokens).flat();

                let values = [];
                for (let i = 0; i < allTokens.length; i++) {
                    const token = getToken(allTokens[i], chainId);
                    const tokenAddress = token.address;
                    const tokenName = token.name;

                    const jewelContract = getContractByName("Jewel", chainId);
                    const jewelPair = getJEWELPair(tokenAddress, chainId);

                    if (jewelPair && jewelPair !== undefined) {
                        const isTokenJewel = tokenAddress.toLowerCase() === jewelPair.token0Address.toLowerCase();
                        const token0 = getToken(jewelPair.token0Address, chainId);
                        const token1 = getToken(jewelPair.token1Address, chainId);
                        const price = await getPairPrice(jewelPair.address, token0.decimals, token1.decimals, chainId, library);
                        const quantity = await getTokenQuantity(tokenAddress.toLowerCase(), account, chainId, library);
                        const formattedQuantity = formatUnits(quantity ? quantity : 0, isTokenJewel ? token0.decimals : token1.decimals);

                        let value = 0;
                        if (isTokenJewel) {
                            value = price ? parseFloat(formattedQuantity * price) : "-";
                        } else {
                            value = price ? parseFloat(formattedQuantity * (1 / price)) : "-";
                        }

                        let valueUsd = "-";
                        const usdPair = getUSDPair(jewelContract.address, chainId);

                        if (usdPair && usdPair !== undefined) {
                            const token0Usd = getToken(usdPair.token0Address, chainId);
                            const token1Usd = getToken(usdPair.token1Address, chainId);
                            const priceJewelUsd = await getPairPrice(usdPair.address, token0Usd.decimals, token1Usd.decimals, chainId, library);
                            valueUsd = priceJewelUsd ? parseFloat(value * priceJewelUsd) : "-";
                        }

                        values.push({
                            valueJewel: value,
                            valueUsd: valueUsd,
                            quantity: formattedQuantity,
                            token: isTokenJewel ? token0 : token1,
                            name: tokenName
                        });
                    } else {
                        const token0 = getToken(tokenAddress, chainId);
                        const quantity = await getTokenQuantity(tokenAddress.toLowerCase(), account, chainId, library);
                        const formattedQuantity = formatUnits(quantity ? quantity : 0, token0.decimals);

                        values.push({
                            valueJewel: "-",
                            valueUsd: "-",
                            quantity: formattedQuantity,
                            token: token0,
                            name: tokenName
                        });
                    }
                }
                setValues(values);
                setLoading(false);
                setFirstLoad(false);
            }
        }
        getData();
    }, [library, chainId, account, tokens, blockNumber]);

    const tokenTypes = Object.keys(tokens);

    if (firstLoad)
        return <div className='TokensWrapper'>
            <h2>Inventory</h2>
            <Card style={{ width: "100%", marginTop: 16 }} loading={true}>
                <Meta
                    avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                    title="Card title"
                    description="This is the description"
                />
            </Card>
        </div>;

    return <div className="TokensWrapper">
        {!loading && <h2>Inventory</h2>}
        {loading && <div style={{ display: "flex", alignItems: "flex-end" }}><h2>Inventory</h2><Spin indicator={antIcon} style={{ marginLeft: "12px", marginBottom: "14px" }} /></div>}
        <Tabs defaultActiveKey="0" onChange={onChageTab}>
            <TabPane tab="All" key="0">
                <TokensList
                    parentKey={"0"}
                    values={values}
                    chainId={chainId}
                    firstLoad={firstLoad}
                />
            </TabPane>
            {tokenTypes && tokenTypes.length > 0 && tokenTypes.map((tokenType, index) => <TabPane tab={tokenType.charAt(0).toUpperCase() + tokenType.slice(1)} key={index + 1}>
                <TokensList
                    parentKey={index + 1}
                    values={values.filter((value) => tokens?.[tokenType]?.map((address) => address.toLowerCase()).includes(value.token?.address?.toLowerCase()))}
                    chainId={chainId}
                    firstLoad={firstLoad}
                />
            </TabPane>)}
        </Tabs>
    </div>
}

export default Tokens;

