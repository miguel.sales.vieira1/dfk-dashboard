import { formatUnits, formatEther } from '@ethersproject/units'
import { getAddress } from "@harmony-js/crypto";
import { SwapRightOutlined } from '@ant-design/icons';
import { getNetworkToken, getTokenDecimals, getTokenName } from '../../utils/api/tokens.js';
import { Card, Avatar } from 'antd';
import moment from "moment";
import { getContractName } from "../../utils/api/contracts.js";
import "./styles.scss";

const { Meta } = Card;

function HistoryList({ transactions, chainId, account, loaded }) {
    const networkToken = getNetworkToken(chainId);

    const Transfer = ({ value, index }) => {

        const fromUrl = "https://explorer.harmony.one/address/" + value.fromAddress;
        const toUrl = "https://explorer.harmony.one/address/" + value.toAddress;

        return <div key={"log" + index} className="LogWrapper">
            <div className="Inputs">
                <div className="InputWrapper" onClick={() => window.open(fromUrl)}>
                    <p className="InputValue">{value.from}</p>
                </div>
                <SwapRightOutlined style={{ fontSize: '24px', color: '#29af57' }} />
                <div className="InputWrapper" onClick={() => window.open(toUrl)} >
                    <p className="InputValue" style={{ marginRight: "40px", marginLeft: "20px" }}>{value.to}</p>
                </div>
                <div className="InputWrapper">
                    <p className="InputValue" style={{
                        background: "unset",
                        border: "unset",
                        padding: "0px",
                        margin: "0px",
                        marginRight: "20px",
                        width: "100px",
                        textAlign: "right"
                    }}>{value.result}</p>
                </div>
                <div className="InputWrapper">
                    <p className="InputValue" style={{ marginRight: "0px" }}>{value.token}</p>
                </div>
            </div>
        </div >
    }

    const Transaction = ({ transaction, index }) => {
        const currDate = moment.unix(transaction.timestamp).format("MMMM Do YYYY");
        const prevDate = index === 0 ? "INVALID" : moment.unix(transactions[index - 1].timestamp).format("MMMM Do YYYY");

        const transferLogs = transaction?.logs?.filter((log) => log.name === "Transfer") || [];
        if (transferLogs.length <= 0)
            return null;

        let totalValues = {};
        let transferValues = [];
        for (let i = 0; i < transferLogs.length; i++) {
            const log = transferLogs[i];
            const value = log.inputs[2].value;
            const units = getTokenDecimals(log.tokenAddress, chainId);
            const result = formatUnits(value, units);

            const isFromYou = log.inputs[0].value.toLowerCase() === getAddress(account).basicHex.toLowerCase();
            const isToYou = log.inputs[1].value.toLowerCase() === getAddress(account).basicHex.toLowerCase();
            const isMint = log.inputs[0].value.toLowerCase() === "0x0000000000000000000000000000000000000000";
            const isBurn = log.inputs[1].value.toLowerCase() === "0x0000000000000000000000000000000000000000";

            const from = isFromYou ? "YOU" : isMint ? "MINT" : getContractName(log.inputs[0].value, chainId);
            const to = isToYou ? "YOU" : isBurn ? "BURN" : getContractName(log.inputs[1].value, chainId);

            const token = getTokenName(log.tokenAddress, chainId);

            const formattedResult = units === 0 ? result : parseFloat(result).toFixed(6);

            transferValues.push({
                from: from,
                fromAddress: log.inputs[0].value.toLowerCase(),
                to: to,
                toAddress: log.inputs[1].value.toLowerCase(),
                result: formattedResult,
                token: token
            })

            if (!totalValues[log.tokenAddress])
                totalValues[log.tokenAddress] = 0
            if (isFromYou)
                totalValues[log.tokenAddress] -= parseFloat(formattedResult);
            if (isToYou)
                totalValues[log.tokenAddress] += parseFloat(formattedResult);
        }

        const transactionFee = parseFloat(formatUnits(transaction.gasUsed, 0) * formatEther(transaction.gasPrice));
        totalValues[networkToken.address] = parseFloat(-transactionFee).toFixed(6);

        return <div key={"transaction" + index} className='TransactionWrapper'>
            {currDate !== prevDate && <h2 className='Date'>{currDate}</h2>}
            <div className="Transaction">
                <p className="Time">{moment.unix(transaction.timestamp).format("LT")}</p>
                <div className="Mark">
                    <div className="Circle1" />
                    <div className="Circle2" />
                </div>
                <div className="InfoWrapper">
                    <div className="TitleWrapper">
                        <p className="Title">{transaction.from.toLowerCase() === account ? "You" : getContractName(transaction.from, chainId)} interacted with {transaction.to.toLowerCase() === account ? "You" : getContractName(transaction.to, chainId)}</p>
                        <div className="GasWrapper">
                            <p className="Title">Transaction fee</p>
                            <p className="Value">{transactionFee.toFixed(6)}</p>
                        </div>
                    </div>
                    <div className="Logs">
                        {transferValues.map((value, i) => {
                            return <Transfer key={"transfer" + i} value={value} index={i} />;
                        })}
                    </div>
                    <div className="TotalValuesWrapper">
                        {Object.keys(totalValues).map((address) => <div className="TokenWrapper" key={"totalValue" + address}>
                            <p className="Name">{getTokenName(address, chainId)}</p>
                            <p className="Quantity">{totalValues[address] > 0 ? "+" + totalValues[address] : totalValues[address]}</p>
                        </div>)}
                    </div>
                </div>
            </div>
        </div>
    }

    if (!loaded)
        return <div className='TokensListWrapper'>
            <h2>Transactions History</h2>
            <Card style={{ width: "100%", marginTop: 16 }} loading={true}>
                <Meta
                    avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                    title="Card title"
                    description="This is the description"
                />
            </Card>
        </div>;
    else
        return <div className='HistoryListWrapper'>
            <h2>Transactions History</h2>
            <div className="TableWrapper">
                <div className="Header">
                    {/* {tokens && tokens.map((token) => {
                    return <p className="Token">
                        {token.name}
                    </p>
                })} */}
                </div>
                <div className="HistoryList">
                    <div className="Line" />
                    {transactions && transactions.length > 0 && transactions.map((transaction, index) => {
                        return <Transaction key={"transaction" + index} transaction={transaction} index={index} />;
                    })}
                </div>
                <div className="Footer">
                    {/* {tokens && tokens.map((token) => {
                    return <p className="Token">
                        {token.name}
                    </p>
                })} */}
                </div>
            </div>

        </div>;
}

export default HistoryList;