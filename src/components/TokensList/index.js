import TokenItem from "../TokenItem";
import "./styles.scss";

function TokensList({ parentKey, values, chainId }) {
    if (!chainId || !values)
        return null;

    const totalValueJewel = values?.map((value) => {
        if (isNaN(value.valueJewel))
            return 0;
        else
            return parseFloat(value.valueJewel)
    }).reduce((totalValue, currentValue) => {
        return parseFloat(totalValue) + parseFloat(currentValue);
    }, 0);

    const totalValueUsd = values?.map((value) => {
        if (isNaN(value.valueUsd))
            return 0;
        else
            return parseFloat(value.valueUsd)
    }).reduce((totalValue, currentValue) => {
        return parseFloat(totalValue) + parseFloat(currentValue);
    }, 0);

    return (<div className='TokensListWrapper'>
        <div className="Tokens">
            {values && values.length > 0 && values.map((value) => {
                return <TokenItem key={parentKey + value.token?.address} token={value.token} quantity={value.quantity} valueJewel={value.valueJewel} valueUsd={value.valueUsd} />
            })}
        </div>
        <div className="TokensListFooter">
            <p className="Value" style={{ flex: "1", textAlign: "start" }}>Total</p>
            <p className="Value">{parseFloat(totalValueJewel).toFixed(4) + " Jewel"}</p>
            <p className="Value">{"$ " + parseFloat(totalValueUsd).toFixed(2)}</p>
        </div>
    </div>)
}

export default TokensList;

