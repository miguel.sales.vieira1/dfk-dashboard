import React, { useState, useEffect } from "react";
import { formatEther } from '@ethersproject/units'
import { Input } from 'antd';
import config from "../../config.json";
import { getTransactionsParsed } from "../../utils/api/transactions.js";
import { getAddress } from "@harmony-js/crypto";

import Main from "../Main";
import Header from "../../components/Header";

import "./styles.scss";

const { Search } = Input;

function App({ chainId, library }) {
    const [accountValue, setAccountValue] = useState(null);
    const [account, setAccount] = useState(null);
    const [mainBalance, setMainBalance] = useState(0);
    const [blockNumber, setBlockNumber] = useState(0);
    const [loading, setLoading] = useState(false);

    const [allStateTransactions, setAllStateTransactions] = useState([]);
    const [currentStateTransactions, setCurrentStateTransactions] = useState([]);



    //const harmonyAddress = getAddress(account).bech32;
    let ethAddress = null
    try {
        ethAddress = getAddress(account).basicHex;
    } catch (error) {
        ethAddress = null;
    }

    useEffect(() => {
        if (ethAddress && loading && library) {
            async function getData() {
                const mainBalance = await library.getBalance(ethAddress);
                const blockNumber = await library.getBlockNumber();
                setMainBalance(mainBalance);
                setBlockNumber(blockNumber);
            }
            getData();
        }
    }, [ethAddress, library, loading]);
    /*************** GET TRANSACTIONS  *****************/
    useEffect(() => {
        if (library && chainId && ethAddress) {
            getTransactionsParsed(ethAddress, chainId, library).then((transactions) => {
                setAllStateTransactions(transactions);
                setCurrentStateTransactions(transactions);
                setLoading(false);
            }).catch((error) => setLoading(false));
        }
    }, [library, chainId, ethAddress, blockNumber, loading]);
    /**************************************************/

    if (!account || !ethAddress || !chainId || !library || !blockNumber || allStateTransactions.length <= 0 || currentStateTransactions.length <= 0)
        return <div className='AppWrapper'>
            <Header
                currentTransactions={currentStateTransactions}
                allTransactions={allStateTransactions}
                setTransactions={setCurrentStateTransactions}
                account={ethAddress}
                chainId={chainId}
            />
            <div className="AccountInfoWrapper">
                <div className="AccountWrapper">
                    <p className="Account">Account</p>
                    <Search
                        placeholder="Input your account"
                        enterButton="Search"
                        size="large"
                        value={accountValue}
                        loading={loading}
                        onChange={(e) => setAccountValue(e.target.value)}
                        onSearch={(value) => {
                            setLoading(true);
                            setAccount(value);
                        }}
                    />
                </div>
            </div>
        </div>;

    //console.log("allStateTransactions", allStateTransactions);
    //console.log("currentStateTransactions", currentStateTransactions);

    return (<div className='AppWrapper'>
        <Header
            currentTransactions={currentStateTransactions}
            allTransactions={allStateTransactions}
            setTransactions={setCurrentStateTransactions}
            account={ethAddress}
            chainId={chainId}
        />
        <div className="AccountInfoWrapper">
            {account && <div className="AccountWrapper">
                <p className="Account">Account</p>
                <Search
                    placeholder="Input your account"
                    enterButton="Search"
                    size="large"
                    value={accountValue}
                    loading={loading}
                    onChange={(e) => setAccountValue(e.target.value)}
                    onSearch={(value) => {
                        setLoading(true);
                        setAccount(value);
                    }}
                />
            </div>}
            {mainBalance && chainId && <div className="BalanceWrapper">
                <p className="Balance">Balance</p>
                <div className="ValueWrapper">
                    <p className="Value">{parseFloat(formatEther(mainBalance)).toFixed(4)}</p>
                    <p className="Ticker">{config["network"][chainId]["ticker"]}</p>
                </div>
            </div>}
        </div>
        <Main
            account={ethAddress}
            transactions={currentStateTransactions}
            chainId={chainId}
            library={library}
            loaded={allStateTransactions?.length > 0}
            blockNumber={blockNumber}
        />
    </div>)
}

export default App;

