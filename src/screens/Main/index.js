import { HistoryList } from "../../components";
import { Row, Col } from 'antd';

import "./styles.scss";
import Tokens from "../../components/Tokens";
import TransactionDetails from "../../components/TransactionDetails";
import MainInfo from "../../components/MainInfo";
import Pools from "../../components/Pools";

function Main({ account, transactions, chainId, library, loaded, blockNumber }) {
    return (<div className='MainWrapper'>
        <Row gutter={[24, 24]}>
            <Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={8}>
                <MainInfo
                    account={account}
                    chainId={chainId}
                    library={library}
                    blockNumber={blockNumber}
                />
            </Col>
            <Col xs={24} sm={4} md={24} lg={12} xl={12} xxl={8}>
                <Tokens
                    account={account}
                    chainId={chainId}
                    library={library}
                    loaded={loaded}
                    blockNumber={blockNumber}
                />
            </Col>
            <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={8}>
                <Pools
                    account={account}
                    chainId={chainId}
                    library={library}
                    blockNumber={blockNumber}
                />
            </Col>
        </Row>
        <Row gutter={[24, 24]}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={12}>
                <HistoryList
                    transactions={transactions}
                    chainId={chainId}
                    account={account}
                    loaded={loaded}
                />
            </Col>
            <Col xs={24} sm={4} md={24} lg={24} xl={24} xxl={12}>
                <TransactionDetails
                    transactions={transactions}
                    chainId={chainId}
                    account={account}
                    loaded={loaded}
                />
            </Col>
        </Row>
    </div>)
}

export default Main;

