import axios from "axios";
import { ethers } from "ethers";
import { getContractAbi, getContractAbiName, getContractName } from "../../utils/api/contracts.js";

const getReceipt = async (transaction, library) => {
    return library.getTransactionReceipt(transaction);
}

export function getTransactions(network, address, pageIndex, pageSize) {

    const url = "https://rpc.s0.t.hmny.io";
    const headers = {
        headers: {
            'Content-Type': 'application/json',
        }
    };
    const body = {
        "jsonrpc": "2.0",
        "method": "hmyv2_getTransactionsHistory",
        "params": [{
            "address": address,
            "pageIndex": pageIndex,
            "pageSize": pageSize,
            "fullTx": true,
            "txType": "ALL",
            "order": "DESC"
        }],
        "id": 1
    };

    return axios({
        method: 'post',
        url: url,
        headers: headers,
        data: body
    });
};

export async function getReceipts(transactions, library) {
    return await Promise.all(transactions.map(transaction => { return getReceipt(transaction, library) }));
}

export async function getNonce(account, library) {
    return library.getTransactionCount(account);
}

export async function getTransactionsParsed(account, chainId, library) {

    try {
        const nonce = await getNonce(account, library);

        let allTransactionsParsed = JSON.parse(localStorage.getItem("customTransactions")) || {};
        let allTransactionsParsedNetwork = allTransactionsParsed[chainId]?.[account];
        if (!allTransactionsParsedNetwork || allTransactionsParsedNetwork === undefined)
            allTransactionsParsedNetwork = [];

        if (allTransactionsParsedNetwork.length >= nonce)
            return allTransactionsParsedNetwork;

        const transactionsResult = await getTransactions(0, account, 0, nonce - allTransactionsParsedNetwork.length);
        const allTransactions = transactionsResult.data.result.transactions;

        if (allTransactions.length <= 0) {
            return [];
        }

        const filteredTransactionsTimestamps = allTransactions.map((transaction) => {
            return transaction.timestamp
        });

        const filteredTransactionsHashes = allTransactions.map((transaction) => {
            return transaction.ethHash
        });

        const filteredTransactionsNonces = allTransactions.map((transaction) => {
            return transaction.nonce
        });

        const filteredTransactionsGasPrices = allTransactions.map((transaction) => {
            return transaction.gasPrice
        });

        const TransactionsObject = Object.assign.apply({}, filteredTransactionsHashes.map((v, i) => {
            return {
                [v]: {
                    timestamp: filteredTransactionsTimestamps[i],
                    nonce: filteredTransactionsNonces[i],
                    gasPrice: filteredTransactionsGasPrices[i]
                }
            }
        }));

        const allReceipts = await getReceipts(filteredTransactionsHashes, library);

        const results = allReceipts.map((receipt) => {
            const abi = getContractAbi(receipt.to.toLowerCase(), chainId);
            const tokenAbi = getContractAbiName("ERC20", chainId);
            const pairAbi = getContractAbiName("UniswapV2Pair", chainId);
            const abis = abi.concat(tokenAbi, pairAbi);
            const filteredAbis = abis.filter((thing, index, self) => self.findIndex(
                t => t.name === thing.name) === index)

            const abiInterface = new ethers.utils.Interface(filteredAbis);
            const logs = receipt.logs;
            const mappedLogs = logs.map((log) => {
                try {
                    const event = abiInterface.getEvent(log.topics[0]);
                    const inputsParsed = event.inputs.map((input) => { return { name: input.name, type: input.type } })
                    const decoded = abiInterface.decodeEventLog(event.name, log.data, log.topics);

                    const inputsFinal = inputsParsed.map((input) => {
                        return {
                            name: input.name, type: input.type, value: decoded[input.name]
                        }
                    });

                    return {
                        name: event.name,
                        tokenAddress: log.address,
                        inputs: inputsFinal
                    }
                } catch (e) {
                    return null;
                }
            });
            const filteredLogs = mappedLogs.filter(function (x) {
                return x !== undefined && x !== null;
            });

            return {
                name: getContractName(receipt.to, chainId),
                from: receipt.from,
                to: receipt.to,
                gasUsed: receipt.gasUsed,
                timestamp: TransactionsObject[receipt.transactionHash].timestamp,
                nonce: TransactionsObject[receipt.transactionHash].nonce,
                gasPrice: TransactionsObject[receipt.transactionHash].gasPrice,
                logs: filteredLogs,
            };
        });
        const parsedResults = results.filter((result) => result);

        const newResults = allTransactionsParsedNetwork.concat(parsedResults);
        if (!allTransactionsParsed[chainId])
            allTransactionsParsed[chainId] = {};
        allTransactionsParsed[chainId][account] = newResults;
        localStorage.setItem("customTransactions", JSON.stringify(allTransactionsParsed));
        return JSON.parse(JSON.stringify(parsedResults));

    } catch (error) {
        console.error("error", error);
    }

}