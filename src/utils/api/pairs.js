import { ethers } from "ethers";
import { formatUnits } from '@ethersproject/units'
import { getTokenBySymbol, getUSDTokens } from "./tokens";
import contracts from "../../assets/contracts.json";
import pairs from "../../assets/pairsInfo.json";

export async function getAllPairsLength() { }

export const getPairPrice = async (address, token0Decimals, token1Decimals, chainId, library) => {
    const pairContractInfo = contracts[chainId]?.find((contract) => contract.name === "UniswapV2Pair");
    const pairInterface = new ethers.utils.Interface(pairContractInfo.abi);

    const pairContract = new ethers.Contract(address, pairInterface, library);
    const reserves = await pairContract.getReserves();

    const reserve0 = formatUnits(reserves.reserve0, token0Decimals);
    const reserve1 = formatUnits(reserves.reserve1, token1Decimals);

    return parseFloat(reserve1 / reserve0);
}

export function getPairFromAddress(address, chainId) {
    return pairs[chainId]?.find((pair) => (pair.address.toLowerCase() === address.toLowerCase()));
}

export function getPair(address0, address1, chainId) {
    return pairs[chainId]?.find((pair) => (pair.token0Address.toLowerCase() === address0.toLowerCase() && pair.token1Address.toLowerCase() === address1.toLowerCase()) ||
        (pair.token1Address.toLowerCase() === address0.toLowerCase() && pair.token0Address.toLowerCase() === address1.toLowerCase()));
}

export function getUSDPair(address, chainId) {
    const usdTokens = getUSDTokens(chainId);
    const usdTokensAddresses = usdTokens.map((token) => token.address.toLowerCase());
    return pairs[chainId]?.find((pair) => (pair.token0Address.toLowerCase() === address.toLowerCase() || pair.token1Address.toLowerCase() === address.toLowerCase()) &&
        (usdTokensAddresses.includes(pair.token0Address.toLowerCase()) || usdTokensAddresses.includes(pair.token1Address.toLowerCase())));
}

export function getJEWELPair(address, chainId) {
    const jewelToken = getTokenBySymbol("JEWEL", chainId);
    return pairs[chainId]?.find((pair) => (pair.token0Address.toLowerCase() === address.toLowerCase() && pair.token1Address.toLowerCase() === jewelToken.address.toLowerCase()) ||
        (pair.token1Address.toLowerCase() === address.toLowerCase() && pair.token0Address.toLowerCase() === jewelToken.address.toLowerCase()));
}

