
const hasMetamask = () => {
    return window.ethereum;
}

export const switchToHarmony = async () => {
    if (!hasMetamask())
        return false;

    try {
        // check if the chain to connect to is installed
        await window.ethereum.request({
            method: 'wallet_switchEthereumChain',
            params: [{ chainId: '0x63564C40' }], // chainId must be in hexadecimal numbers
        });
    } catch (error) {
        // This error code indicates that the chain has not been added to MetaMask
        // if it is not, then install it into the user MetaMask
        const data = [{
            chainId: '0x63564C40',
            chainName: 'Harmony Mainnet Shard 0',
            nativeCurrency:
            {
                name: 'ONE',
                symbol: 'ONE',
                decimals: 18
            },
            rpcUrls: ['https://api.harmony.one/'],
            blockExplorerUrls: ['https://explorer.harmony.one/'],
        }]

        if (error.code === 4902) {
            try {
                await window.ethereum.request({
                    method: 'wallet_addEthereumChain',
                    params: data,
                });
            } catch (addError) {
                console.error("error2", addError);
            }
        }
        console.error("error1", error);
    }
}
