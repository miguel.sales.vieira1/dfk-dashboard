import { ethers } from "ethers";
import tokens from "../../assets/tokensInfo.json";
import tokenAddresses from "../../assets/tokens.json";
import { getContractByName } from "./contracts";

// export const getTokens = async (chainId, library, setTokensPercentage) => {
//     const tokenContractInfo = contracts[chainId]?.find((contract) => contract.name === "ERC20");
//     const tokenInterface = new ethers.utils.Interface(tokenContractInfo.abi);

//     let currentTokens = JSON.parse(localStorage.getItem("tokens")) || {};
//     let currentTokensArray = currentTokens[chainId] || [];

//     const allTokenAddresses = Object.values(tokens[chainId]).flat();
//     const currentTokenAddresses = currentTokensArray?.map((token) => token.address.toLowerCase());

//     for (let i = 0; i < allTokenAddresses.length; i++) {
//         const tokenAddress = allTokenAddresses[i];
//         if (currentTokenAddresses?.includes(tokenAddress.toLowerCase())) {
//             const percentage = parseInt((i * 100) / allTokenAddresses.length);
//             setTokensPercentage(percentage);
//             continue;
//         }

//         const tokenContract = new ethers.Contract(tokenAddress, tokenInterface, library);

//         const name = await tokenContract.name();
//         const symbol = await tokenContract.symbol();
//         const decimals = await tokenContract.decimals();

//         currentTokensArray.push({
//             address: tokenAddress,
//             name: name,
//             symbol: symbol,
//             decimals: decimals,
//         });

//         currentTokens[chainId] = currentTokensArray;
//         localStorage.setItem("tokens", JSON.stringify(currentTokens));
//         const percentage = parseInt((i * 100) / allTokenAddresses.length);
//         setTokensPercentage(percentage);
//         continue;
//     }
//     return true;
// }

export function getNetworkToken(chainId) {
    const address = tokenAddresses[chainId]?.["network"];
    return tokens[chainId]?.find((token) => token.address.toLowerCase() === address.toLowerCase());
}

export function getCustomTokenAddresses(chainId) {
    const mandatoryTokens = ["network", "main", "usd"];
    const customTokens = Object.keys(tokenAddresses[chainId]).filter((key) => !mandatoryTokens.includes(key));
    let customTokenAddresses = {};
    for (let i = 0; i < customTokens.length; i++) {
        customTokenAddresses[customTokens[i]] = tokenAddresses[chainId][customTokens[i]];
    }
    return customTokenAddresses;
}

export const getTokenQuantity = async (address, account, chainId, library) => {
    const tokenContractInfo = getContractByName("ERC20", chainId);
    const tokenInterface = new ethers.utils.Interface(tokenContractInfo.abi);
    const tokenContract = new ethers.Contract(address, tokenInterface, library);
    const balance = await tokenContract.balanceOf(account);
    return balance;
}

export const getLockedJewelQuantity = async (account, chainId, library) => {
    const jewelInfo = getContractByName("Jewel", chainId);
    const jewelInterface = new ethers.utils.Interface(jewelInfo.abi);
    const jewelContract = new ethers.Contract(jewelInfo.address, jewelInterface, library);
    const lockedJewel = await jewelContract.lockOf(account);
    return lockedJewel;
}

export function getToken(address, chainId) {
    if (!address) return null;
    return tokens[chainId]?.find((token) => token.address.toLowerCase() === address.toLowerCase());
}

export function getUSDTokens(chainId) {
    return tokens[chainId]?.filter((token) => token.symbol.toLowerCase().includes("usd"));
}

export function getTokenName(address, chainId) {
    const name = tokens[chainId]?.find((token) => { return token.address.toLowerCase() === address.toLowerCase() })?.name;
    return name ? name : address;
}

export function getTokenSymbol(address, chainId) {
    const ticker = tokens[chainId]?.find((token) => { return token.address.toLowerCase() === address.toLowerCase() })?.symbol;
    return ticker ? ticker : address;
}

export function getTokenDecimals(address, chainId) {
    const units = tokens[chainId]?.find((token) => { return token.address.toLowerCase() === address.toLowerCase() })?.decimals;
    return units === 0 || units ? units : 18;
}

export function getTokenBySymbol(symbol, chainId) {
    const token = tokens[chainId]?.find((token) => { return token.symbol === symbol });
    return token;
}

