import contracts from "../../assets/contracts.json";
import { getToken } from "./tokens";
import { getPairFromAddress } from "./pairs";

export function getContractByName(name, chainId) {
    return contracts[chainId]?.find((contract) => contract.name === name);
}

export function getCustomContracts(chainId) {
    const mandatoryContracts = ["ERC20", "UniswapV2Factory", "UniswapV2Pair", "Pools", "Jewel"];
    return contracts[chainId]?.filter((contract) => !mandatoryContracts.includes(contract.name));
}

export function getContractName(address, chainId) {
    const name = contracts[chainId]?.find((contract) => contract.address?.toLowerCase() === address.toLowerCase())?.name;
    return name ? name : address
}

export function getContractAbiName(name, chainId) {
    return contracts[chainId]?.find((contract) => contract.name === name)?.abi;
}

export function getContractAbi(address, chainId) {
    const contractsWithAddress = getCustomContracts(chainId);
    if (contractsWithAddress && contractsWithAddress.length > 0) {
        const contractFound = contractsWithAddress.find((contract) => contract.address.toLowerCase() === address.toLowerCase());
        if (contractFound)
            return contractFound.abi;
    }
    const isToken = getToken(address, chainId);
    if (isToken) {
        const tokenContract = contracts[chainId]?.find((contract) => contract.name === "ERC20");
        return tokenContract.abi;
    }
    const isPair = getPairFromAddress(address, chainId);
    if (isPair) {
        const pairContract = contracts[chainId]?.find((contract) => contract.name === "UniswapV2Pair");
        return pairContract.abi;
    }
    return [];
}
