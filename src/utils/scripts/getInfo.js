const fs = require('fs');
var path = require('path');
const ethers = require('ethers');
const cliProgress = require('cli-progress');
const { formatUnits } = require('@ethersproject/units');
const contracts = require("../../assets/contracts.json");
const tokens = require("../../assets/tokens.json");

const chainId = "1666600000";
const library = new ethers.providers.JsonRpcProvider("https://api.s0.t.hmny.io/");

const tokensInfoFileLocationv = "../../assets/tokensInfo.json";
const pairsInfoFileLocationv = "../../assets/pairsInfo.json";

const tokensBar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
const pairsBar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);

async function getPairs() {
    console.log("Getting pairs");
    const factoryContractInfo = contracts[chainId]?.find((contract) => contract.name === "UniswapV2Factory");
    const pairContractInfo = contracts[chainId]?.find((contract) => contract.name === "UniswapV2Pair");

    const factoryInterface = new ethers.utils.Interface(factoryContractInfo.abi);
    const pairInterface = new ethers.utils.Interface(pairContractInfo.abi);

    let currentPairs = {};
    try {
        currentPairs = JSON.parse(fs.readFileSync(path.join(__dirname, pairsInfoFileLocationv), 'utf8'));
    } catch (error) {
    }

    let currentPairsArray = currentPairs[chainId] || [];

    const currentPairAddresses = currentPairsArray.map((pair) => pair.address.toLowerCase());

    const factoryContract = new ethers.Contract(factoryContractInfo.address, factoryInterface, library);
    const allPairsLength = await factoryContract.allPairsLength();
    const formattedAllPairsLength = formatUnits(allPairsLength, 0);

    pairsBar.start(formattedAllPairsLength, 0);
    for (let i = 0; i < formattedAllPairsLength; i++) {
        const pairAddress = await factoryContract.allPairs(i);
        if (currentPairAddresses.includes(pairAddress.toLowerCase())) {
            pairsBar.update(i + 1);
            continue;
        }

        const pairContract = new ethers.Contract(pairAddress, pairInterface, library);

        const token0Address = await pairContract.token0();
        const token1Address = await pairContract.token1();
        const decimals = await pairContract.decimals();

        const pair = {
            address: pairAddress,
            decimals: decimals,
            token0Address: token0Address,
            token1Address: token1Address,
        };

        currentPairsArray.push((pair));
        currentPairs[chainId] = currentPairsArray;

        fs.writeFile(path.join(__dirname, pairsInfoFileLocationv), JSON.stringify(currentPairs), err => {
            if (err) {
                console.log('Error writing file', err)
            }
        })
        pairsBar.update(i + 1);
    }
    pairsBar.stop();
}

async function getTokens() {
    console.log("Getting tokens");
    const tokenContractInfo = contracts[chainId]?.find((contract) => contract.name === "ERC20");
    const tokenInterface = new ethers.utils.Interface(tokenContractInfo.abi);

    let currentPairs = {};
    let currentTokens = {};
    try {
        currentPairs = JSON.parse(fs.readFileSync(path.join(__dirname, pairsInfoFileLocationv), 'utf8'));
    } catch (error) {
    }
    try {
        currentTokens = JSON.parse(fs.readFileSync(path.join(__dirname, tokensInfoFileLocationv), 'utf8'));
    } catch (error) {
    }
    let currentTokensArray = currentTokens[chainId] || [];

    const customTokenAddresses = Object.values(tokens[chainId]).flat();
    const token0Addresses = currentPairs[chainId].map((pair) => pair.token0Address);
    const token1Addresses = currentPairs[chainId].map((pair) => pair.token1Address);
    const allTokensArray = customTokenAddresses.concat(token0Addresses, token1Addresses);
    const allTokenAddresses = [...new Set(allTokensArray)];

    const currentTokenAddresses = currentTokensArray?.map((token) => token.address.toLowerCase());
    tokensBar.start(allTokenAddresses.length, 0);
    for (let i = 0; i < allTokenAddresses.length; i++) {
        const tokenAddress = allTokenAddresses[i];
        if (currentTokenAddresses?.includes(tokenAddress.toLowerCase())) {
            tokensBar.update(i + 1);
            continue;
        }

        const tokenContract = new ethers.Contract(tokenAddress, tokenInterface, library);

        let name = await tokenContract.name();
        let symbol = await tokenContract.symbol();
        let decimals = await tokenContract.decimals();

        currentTokensArray.push({
            address: tokenAddress,
            name: name,
            symbol: symbol,
            decimals: decimals,
        });

        currentTokens[chainId] = currentTokensArray;
        fs.writeFile(path.join(__dirname, tokensInfoFileLocationv), JSON.stringify(currentTokens), err => {
            if (err) {
                console.log('Error writing file', err)
            }
        });
        tokensBar.update(i + 1);
    }
    tokensBar.stop();
}

async function start() {
    await getPairs();
    await getTokens();
}

start();

